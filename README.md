# React-Redux-Calculator

A simple calculator app created using **react** and **redux**.

The app is bootstrapped using `create-react-app`

The functionalities of the calculator are implemented using **redux actions** and **redux reducers** and the state of the calculator is maintained as **redux store**.
