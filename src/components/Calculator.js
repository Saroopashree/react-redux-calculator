import React from "react";

import Display from "./Display";
import Keypad from "./Keypad";

import "../stylesheets/Calculator.css";

function Calculator() {
  return (
    <div>
      <h2 className="title">Calculator 2kXX</h2>
      <div className="calculator">
        <Display />
        <Keypad />
      </div>
    </div>
  );
}

export default Calculator;
