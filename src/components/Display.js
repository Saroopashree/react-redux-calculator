import React from "react";
import { useSelector } from "react-redux";

import "../stylesheets/Display.css";

function Display() {
  const isPoweredOn = useSelector(state => state.isPoweredOn);
  const displayString = useSelector(state => state.display);

  return (
    <div className="display display-text">{isPoweredOn && displayString}</div>
  );
}

export default Display;
