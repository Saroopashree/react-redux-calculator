import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDispatch } from "react-redux";

import "../stylesheets/Key.css";

function Key(props) {
  const { value, icon, gridPlacement, bgColor, actionCreator } = props.keyProp;
  const dispatch = useDispatch();
  return (
    <button
      className="key centerify-self centerify-content"
      style={{
        gridColumn: gridPlacement.column,
        gridRow: gridPlacement.row,
        background: bgColor
      }}
      onClick={() => dispatch(actionCreator())}
    >
      {value || <FontAwesomeIcon icon={icon} />}
    </button>
  );
}

export default Key;
