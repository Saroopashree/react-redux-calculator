import {
  faLongArrowAltLeft,
  faTimes,
  faDivide,
  faMinus,
  faPlus,
  faEquals,
  faPowerOff
} from "@fortawesome/free-solid-svg-icons";

import {
  clearAll,
  delete_,
  divide,
  typeNumber,
  multiply,
  subtract,
  add,
  equals,
  inverse,
  power
} from "../redux/actions/actionCreators";

export const keys = [
  {
    value: "",
    icon: faPowerOff,
    gridPlacement: {
      column: "1",
      row: "1"
    },
    bgColor: "#d11d1d",
    actionCreator: power
  },
  {
    value: "AC",
    icon: "",
    gridPlacement: {
      column: "2",
      row: "1"
    },
    bgColor: "#fca503",
    actionCreator: clearAll
  },
  {
    value: "",
    icon: faLongArrowAltLeft,
    gridPlacement: {
      column: "3",
      row: "1"
    },
    bgColor: "#fca503",
    actionCreator: delete_
  },
  {
    value: "",
    icon: faDivide,
    gridPlacement: {
      column: "4",
      row: "1"
    },
    bgColor: "#fca503",
    actionCreator: divide
  },
  {
    value: "7",
    icon: "",
    gridPlacement: {
      column: "1",
      row: "2"
    },
    bgColor: "#eaeeea",
    actionCreator: () => typeNumber("7")
  },
  {
    value: "8",
    icon: "",
    gridPlacement: {
      column: "2",
      row: "2"
    },
    bgColor: "#eaeeea",
    actionCreator: () => typeNumber("8")
  },
  {
    value: "9",
    icon: "",
    gridPlacement: {
      column: "3",
      row: "2"
    },
    bgColor: "#eaeeea",
    actionCreator: () => typeNumber("9")
  },
  {
    value: "",
    icon: faTimes,
    gridPlacement: {
      column: "4",
      row: "2"
    },
    bgColor: "#fca503",
    actionCreator: multiply
  },
  {
    value: "4",
    icon: "",
    gridPlacement: {
      column: "1",
      row: "3"
    },
    bgColor: "#eaeeea",
    actionCreator: () => typeNumber("4")
  },
  {
    value: "5",
    icon: "",
    gridPlacement: {
      column: "2",
      row: "3"
    },
    bgColor: "#eaeeea",
    actionCreator: () => typeNumber("5")
  },
  {
    value: "6",
    icon: "",
    gridPlacement: {
      column: "3",
      row: "3"
    },
    bgColor: "#eaeeea",
    actionCreator: () => typeNumber("6")
  },
  {
    value: "",
    icon: faMinus,
    gridPlacement: {
      column: "4",
      row: "3"
    },
    bgColor: "#fca503",
    actionCreator: subtract
  },
  {
    value: "1",
    icon: "",
    gridPlacement: {
      column: "1",
      row: "4"
    },
    bgColor: "#eaeeea",
    actionCreator: () => typeNumber("1")
  },
  {
    value: "2",
    icon: "",
    gridPlacement: {
      column: "2",
      row: "4"
    },
    bgColor: "#eaeeea",
    actionCreator: () => typeNumber("2")
  },
  {
    value: "3",
    icon: "",
    gridPlacement: {
      column: "3",
      row: "4"
    },
    bgColor: "#eaeeea",
    actionCreator: () => typeNumber("3")
  },
  {
    value: "",
    icon: faPlus,
    gridPlacement: {
      column: "4",
      row: "4"
    },
    bgColor: "#fca503",
    actionCreator: add
  },
  {
    value: "+/-",
    icon: "",
    gridPlacement: {
      column: "1",
      row: "5"
    },
    bgColor: "#fca503",
    actionCreator: inverse
  },
  {
    value: "0",
    icon: "",
    gridPlacement: {
      column: "2",
      row: "5"
    },
    bgColor: "#eaeeea",
    actionCreator: () => typeNumber("0")
  },
  {
    value: ".",
    icon: "",
    gridPlacement: {
      column: "3",
      row: "5"
    },
    bgColor: "#eaeeea",
    actionCreator: () => typeNumber(".")
  },
  {
    value: "",
    icon: faEquals,
    gridPlacement: {
      column: "4",
      row: "5"
    },
    bgColor: "#fca503",
    actionCreator: equals
  }
];
