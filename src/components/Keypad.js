import React from "react";

import Key from "./Key";

import { keys } from "./keys";

import "../stylesheets/Keypad.css";

function Keypad() {
  const generateKeys = () => {
    return keys.map(key => <Key keyProp={key} />);
  };

  return <div className="keypad keypad-grid">{generateKeys()}</div>;
}

export default Keypad;
