import {
  ADD,
  SUBTRACT,
  MULTIPLY,
  DIVIDE,
  TYPE_NUMBER,
  EQUALS,
  CLEAR_ALL,
  DELETE,
  INVERSE,
  POWER
} from "../actions/actionTypes";
import initialState from "./initState";

const replaceOperator = (string, operator) => {
  // When an operator is clicked after an operator, the recent operator will replace the old one

  return string.slice(0, string.length - 1) + operator;
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case POWER:
      // During power ON and OFF clear everything to initialState and toggle the isPoweredOn bool
      return {
        ...initialState,
        isPoweredOn: !state.isPoweredOn
      };

    case ADD:
      if (!state.isPoweredOn) {
        // When powered OFF return the previous state
        return state;
      }
      if (state.previousKey === "update" || state.previousKey === "clear") {
        // If previous key was an "update" or "clear" key
        var new_display_str = state.display + "+";
      } else if (state.previousKey === "operator") {
        // If th previous key was an "operator"
        new_display_str = replaceOperator(state.display, "+");
      } else {
        new_display_str = state.display;
      }
      return {
        ...state,
        display: new_display_str,
        previousKey: action.payload.keyType,
        currentNum: ""
      };

    case SUBTRACT:
      if (!state.isPoweredOn) {
        // When powered OFF return the previous state
        return state;
      }
      if (state.previousKey === "update" || state.previousKey === "clear") {
        // If previous key was an "update" or "clear" key
        new_display_str = state.display + "-";
      } else if (state.previousKey === "operator") {
        // If th previous key was an "operator"
        new_display_str = replaceOperator(state.display, "-");
      } else {
        new_display_str = state.display;
      }
      return {
        ...state,
        display: new_display_str,
        previousKey: action.payload.keyType,
        currentNum: ""
      };

    case MULTIPLY:
      if (!state.isPoweredOn) {
        // When powered OFF return the previous state
        return state;
      }
      if (state.previousKey === "update" || state.previousKey === "clear") {
        // If previous key was an "update" or "clear" key
        new_display_str = state.display + "*";
      } else if (state.previousKey === "operator") {
        // If th previous key was an "operator"
        new_display_str = replaceOperator(state.display, "*");
      } else {
        new_display_str = state.display;
      }
      return {
        ...state,
        display: new_display_str,
        previousKey: action.payload.keyType,
        currentNum: ""
      };

    case DIVIDE:
      if (!state.isPoweredOn) {
        // When powered OFF return the previous state
        return state;
      }
      if (state.previousKey === "update" || state.previousKey === "clear") {
        // If previous key was an "update" or "clear" key
        new_display_str = state.display + "/";
      } else if (state.previousKey === "operator") {
        // If th previous key was an "operator"
        new_display_str = replaceOperator(state.display, "/");
      } else {
        new_display_str = state.display;
      }
      return {
        ...state,
        display: new_display_str,
        previousKey: action.payload.keyType,
        currentNum: ""
      };

    case TYPE_NUMBER:
      if (!state.isPoweredOn) {
        // When powered OFF return the previous state
        return state;
      }
      new_display_str = state.display;
      var currNum = state.currentNum;
      var prevKey = action.payload.keyType;
      if (
        (action.payload.input === "." && state.currentNum.includes(".")) ||
        state.display.length >= 11
      ) {
        // Blocks the display from unpdating the input if the incomming value is "." when there already exists a "." for the same number
        // Blocks the display from updating the input if the length of the display string exceeds '11' characters
      } else if (
        state.previousKey === "update" ||
        state.previousKey === "operator" ||
        action.payload.input === "."
      ) {
        if (state.currentNum === "0" && action.payload.input !== ".") {
          // Prevents entries like "0345"
          // Slices off the first 'zero'
          new_display_str =
            new_display_str.slice(0, new_display_str.length - 1) +
            action.payload.input;
          currNum = action.payload.input;
        } else if (state.currentNum !== "0" || action.payload.input !== "0") {
          // Append the input to the display string
          new_display_str = new_display_str + action.payload.input;
          currNum = currNum + action.payload.input;
        }
      } else if (state.previousKey === "clear") {
        // Prevents entries like "0345" when the previous key was of type "clear"
        new_display_str = action.payload.input;
        currNum = action.payload.input;
      }
      return {
        ...state,
        display: new_display_str,
        previousKey: prevKey,
        currentNum: currNum
      };

    case EQUALS:
      if (!state.isPoweredOn) {
        // When powered OFF return the previous state
        return state;
      }
      if (state.previousKey === "operator") {
        // When last character of the display string is an operator, slice off the last char and evaluate the expression
        let slicedString = state.display.slice(0, state.display.length - 1);
        if (slicedString === "") {
          var answer = "0";
        } else {
          answer = (+eval(slicedString).toFixed(3)).toString();
        }
      } else {
        answer = (+eval(state.display).toFixed(3)).toString();
      }
      return {
        ...state,
        display: answer,
        previousKey: action.payload.keyType,
        currentNum: answer
      };

    case CLEAR_ALL:
      if (!state.isPoweredOn) {
        // When powered OFF return the previous state
        return state;
      }
      // ClearAll just clears the state to the initialState
      return initialState;

    case DELETE:
      if (!state.isPoweredOn) {
        // When powered OFF return the previous state
        return state;
      }
      if (state.display.length === 1) {
        // If the display string has only one character, clear everything to initialState
        new_display_str = "0";
        prevKey = "clear";
        currNum = "";
      } else {
        // If the display string has multiple characters, slice off the last character
        new_display_str = state.display.slice(0, state.display.length - 1);
        if (new_display_str === "0") {
          prevKey = "clear";
          currNum = "";
        } else if (
          new_display_str[new_display_str.length - 1].match(/[\+\*-\/]/g) !==
          null
        ) {
          // Last character of new_display_str is an operator, Update currNum to ""
          prevKey = "operator";
          currNum = "";
        } else {
          // Last character of new_display_str is a number, Update currNum to the last operand of the current expression
          prevKey = "update";
          let operands = new_display_str.split(/[\+\*-\/]/g);
          currNum = operands[operands.length - 1];
        }
      }
      return {
        ...state,
        display: new_display_str,
        previousKey: prevKey,
        currentNum: currNum
      };

    case INVERSE:
      if (!state.isPoweredOn) {
        // When powered OFF return the previous state
        return state;
      }
      if (state.display[0] === "-") {
        // If "-" is already present, slice if off
        new_display_str = state.display.slice(1, state.display.length);
      } else {
        // If "-" is not present, add it to the front of the display string
        new_display_str = "-" + state.display;
      }
      return {
        ...state,
        display: new_display_str
      };

    default:
      // To set the initial state of the store
      return state;
  }
};

export default rootReducer;
