const initialState = {
  isPoweredOn: true,
  display: "0",
  previousKey: "clear",
  currentNum: ""
};

export default initialState;
