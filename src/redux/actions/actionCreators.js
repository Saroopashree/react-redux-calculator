import {
  ADD,
  SUBTRACT,
  MULTIPLY,
  DIVIDE,
  EQUALS,
  CLEAR_ALL,
  DELETE,
  TYPE_NUMBER,
  INVERSE,
  POWER
} from "./actionTypes";

export const add = () => {
  return {
    type: ADD,
    payload: {
      keyType: "operator"
    }
  };
};

export const subtract = () => {
  return {
    type: SUBTRACT,
    payload: {
      keyType: "operator"
    }
  };
};

export const multiply = () => {
  return {
    type: MULTIPLY,
    payload: {
      keyType: "operator"
    }
  };
};

export const divide = () => {
  return {
    type: DIVIDE,
    payload: {
      keyType: "operator"
    }
  };
};

export const equals = () => {
  return {
    type: EQUALS,
    payload: {
      keyType: "update"
    }
  };
};

export const clearAll = () => {
  return {
    type: CLEAR_ALL,
    payload: {
      keyType: "clear"
    }
  };
};

export const delete_ = () => {
  return {
    type: DELETE,
    payload: {
      keyType: "delete"
    }
  };
};

export const typeNumber = value => {
  return {
    type: TYPE_NUMBER,
    payload: {
      keyType: "update",
      input: value
    }
  };
};

export const inverse = () => {
  return {
    type: INVERSE,
    payload: {
      keyType: "inverse"
    }
  };
};

export const power = () => {
  return {
    type: POWER,
    payload: {
      keyType: "clear"
    }
  };
};
