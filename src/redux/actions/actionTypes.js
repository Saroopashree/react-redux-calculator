export const ADD = "ADD";
export const SUBTRACT = "SUBTRACT";
export const MULTIPLY = "MULTIPLY";
export const DIVIDE = "DIVIDE";
export const EQUALS = "EQUALS";
export const CLEAR_ALL = "CLEAR_ALL";
export const DELETE = "DELETE";
export const TYPE_NUMBER = "TYPE_NUMBER";
export const INVERSE = "INVERSE";

export const POWER = "POWER";
